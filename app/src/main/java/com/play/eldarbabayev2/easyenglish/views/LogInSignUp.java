package com.play.eldarbabayev2.easyenglish.views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.play.eldarbabayev2.easyenglish.R;

/**
 * Created by eldarbabayev on 14/01/2016.
 */
public class LogInSignUp extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_in_sign_up);
    }

    public void LogInActivityCommand(View v) {
        Intent logInActivity = new Intent(LogInSignUp.this, Login.class);
        startActivity(logInActivity);
    }

    public void SignUpActivityCommand(View v) {
        Intent signUpActivity = new Intent(LogInSignUp.this, SignUp.class);
        startActivity(signUpActivity);
    }

}
